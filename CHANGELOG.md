# Change log

## 0.1.7

- Добавлен bs компонент ListGroup
- Добавлен slim компонент SlimSearch
- MainLayout задана минимальная высота
- Добавлен экспорт переменных, функций и миксинов bs

## 0.1.6

- Slim компоненты наследуются от общего SlimComponent
- Цвета для slim компонентов обобщены
- Поправлены стили связанные с FA
- Переработал компонент меню
- Завершен компонент SlimDatepciker

## 0.1.5

- Цвета для bs компонентов обобщены
- Добавлены bs компоненты:
  - Progress
- Добавлена зависимость `tomi:upload-jquery` для работы с загрузкой файлов
- Добавлены slim-компоненты:
  - SlimProgress
  - SlimUploader
- Создан компонент SlimDatepciker [в разработке]

## 0.1.4

- Доработаны bs компоненты:
  - Nav
  - NavLink
- Добавлены slim-компоненты:
  - SlimNav

## 0.1.3

- Добавлены шрифты из гугла
- Добавлен bs компонент ButtonGroup
- Мелкие правки по bs компонентам
- Добавлены slim-компоненты:
  - SlimCard
  - SlimCardHeader
  - SlimSquare
  - SlimTrend

## 0.1.2

- Добавлен bs компонент Dropdown

## 0.1.1

- Доработаны bs компоненты:
  - CardContent
  - Button
  - Modal
- Добавлены bs компонент Tooltip


## 0.1.0

- Добавлен bootstrap
- Добавлен slim
- Добавлен конструктор компонента
- Добавлены bs компоненты:
  - Alert
  - Button
  - Card
  - CardContent
  - CardLayout
  - Col
  - Row
  - Container
  - Table
  - Modal
  - ModalBody
  - ModalFooter
  - ModalHeader
- Добавлены лайауты:
  - BlankLayout
  - ErrorLayout
  - MainLayout
  - SimpleLayout
  - SplitLayout
- Добавлены slim-компоненты:
  - SlimNavbar
  - SlimPageHeader
  - SlimSectionWrapper
  
## 0.0.1

Создание пакета
