import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../component';
// import template
import './alert.html';
// import bs js
import '../src/alert';

const Alert = new Component('Alert', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    color: {
      type: String,
      defaultValue: 'primary',
      allowedValues: Component.colors,
    },
    dismiss: { type: Boolean, defaultValue: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        color, className, attributes, dismiss,
      } = this.props;
      const classes = c({
        [this.cp]: true,
        [`${this.cp}-${color}`]: true,
        [`${this.cp}-dismissible`]: dismiss,
        'fade show': dismiss,
        [`${className}`]: className,
      });
      return {
        role: 'alert',
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    // class parent
    cp: 'alert',
  },
});

export { Alert as default };
