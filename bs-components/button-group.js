import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../component';
// import template
import './button-group.html';

const ButtonGroup = new Component('ButtonGroup', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    size: { type: String, optional: true, allowedValues: ['sm', 'lg'] },
    vertical: { type: Boolean, defaultValue: false },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        size, vertical, className, attributes,
      } = this.props;
      const classes = c(
        vertical ? `${this.cp}-vertical` : this.cp,
        {
          [`${this.cp}-${size}`]: size,
          [`${className}`]: className,
        },
      );
      return {
        role: 'alert',
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'btn-group',
  },
});

export default ButtonGroup;
