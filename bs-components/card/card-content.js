import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../../component';
// import template
import './card-content.html';

const CardContent = new Component('CardContent', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    type: {
      type: String,
      allowedValues: ['body', 'footer', 'header', 'overlay', 'link', 'img', 'text', 'title', 'subtitle'],
    },
    href: {
      type: String,
      optional: true,
      autoValue() {
        if (this.field('type').value === 'link' && !this.isSet) {
          return '#';
        }
      },
    },
    // todo@aspirin добавить выбор размера заголовка 1..6
    onClick: { type: Function, optional: true },
    src: { type: String, optional: true },
    alt: { type: String, optional: true },
    place: { type: String, optional: true, allowedValues: ['top', 'bottom'] },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    isLink() {
      return this.props.type === 'link';
    },
    isImg() {
      return this.props.type === 'img';
    },
    isText() {
      return this.props.type === 'text';
    },
    isTitle() {
      return this.props.type === 'title';
    },
    isSubtitle() {
      return this.props.type === 'subtitle';
    },
    isDiv() {
      return ['body', 'footer', 'header', 'overlay'].some(type => this.props.type === type);
    },
    attributes() {
      const {
        type, href, src, alt, place, className, attributes,
      } = this.props;
      const classes = c({
        [`${this.cp}-body`]: type === 'body',
        [`${this.cp}-footer`]: type === 'footer',
        [`${this.cp}-header`]: type === 'header',
        [`${this.cp}-img-overlay`]: type === 'overlay',
        [`${this.cp}-link`]: type === 'link',
        [`${this.cp}-img${(place && `-${place}`) || ''}`]: type === 'img',
        [`${this.cp}-text`]: type === 'text',
        [`${this.cp}-title`]: type === 'title',
        [`${this.cp}-subtitle`]: type === 'subtitle',
        [`${className}`]: className,
      });
      return {
        href,
        src,
        alt,
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'click a'(e) {
      if (this.props.onClick) {
        e.preventDefault();
        this.props.onClick();
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    // class parent
    cp: 'card',
  },
});

export default CardContent;
