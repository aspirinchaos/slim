import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../../component';
// import template
import './card-layout.html';

const CardLayout = new Component('CardLayout', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    columns: { type: Boolean, defaultValue: false },
    deck: { type: Boolean, defaultValue: false },
    group: { type: Boolean, defaultValue: false },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        className, attributes, columns, deck, group,
      } = this.props;
      const classes = c({
        [`${this.cp}-columns`]: columns,
        [`${this.cp}-deck`]: deck,
        [`${this.cp}-group`]: group,
        [`${className}`]: className,
      });
      return {
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    // class parent
    cp: 'card',
  },
});

export default CardLayout;
