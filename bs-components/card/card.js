import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../../component';
// import template
import './card.html';

const Card = new Component('Card', {
  // Validate the properties passed to the template from parents
  // todo усли поле уходит из props, то данные остаются последние которые были @see cartProductItem
  props: new SimpleSchema({
    color: {
      type: String,
      optional: true,
      defaultValue: '',
      allowedValues: ['', ...Component.colors],
    },
    // todo@aspirin переделать выбор цвета на bg и text и т.д.
    outline: { type: Boolean, defaultValue: false },
    inverse: { type: Boolean, defaultValue: false },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        color, className, attributes, inverse, outline,
      } = this.props;
      const classes = c({
        [this.cp]: true,
        [`${className}`]: className,
        'text-white': inverse,
        [`${outline ? 'border' : 'bg'}-${color}`]: color,
      });
      return {
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    // class parent
    cp: 'card',
  },
});

export default Card;
