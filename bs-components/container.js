import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../component';
// import template
import './container.html';

const Container = new Component('Container', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    fluid: { type: Boolean, defaultValue: false },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        fluid, className, attributes,
      } = this.props;
      const classes = c({
        [`${this.cp}${fluid ? '-fluid' : ''}`]: true,
        [`${className}`]: className,
      });
      return {
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'container',
  },
});

export default Container;
