import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import c from 'classnames';
// import template
import './dropdown-item.html';

const DropdownItemSchema = new SimpleSchema({
  active: {
    type: Boolean,
    defaultValue: false,
  },
  disabled: {
    type: Boolean,
    defaultValue: false,
  },
  divider: {
    type: Boolean,
    defaultValue: false,
  },
  header: {
    type: Boolean,
    defaultValue: false,
  },
  text: {
    type: Boolean,
    defaultValue: false,
  },
  onClick: {
    type: Function,
    optional: true,
  },
  content: {
    type: String,
    defaultValue: '',
  },
  // для работы dropdown-item как селект option
  value: {
    type: String,
    optional: true,
  },
});

TemplateController('DropdownItem', {
  // Validate the properties passed to the template from parents
  props: DropdownItemSchema,

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    divider() {
      return {
        class: `${this.cp}-divider`,
      };
    },
    text() {
      return {
        class: `${this.cp}-item-text`,
      };
    },
    header() {
      return {
        class: `${this.cp}-header`,
      };
    },
    isItem() {
      const { divider, text, header } = this.props;
      return !(divider || text || header);
    },
    item() {
      const { active, disabled } = this.props;
      return {
        class: c({
          [`${this.cp}-item`]: true,
          active,
          disabled,
        }),
        href: '#',
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'click a'(e) {
      const { onClick, content, value } = this.props;
      e.preventDefault();
      onClick(value, content);
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'dropdown',
  },
});

export default DropdownItemSchema;
