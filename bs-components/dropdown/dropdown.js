import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../../component';
import DropdownItemSchema from './dropdown-item';
// import template
import './dropdown.html';
// import bs js
import '../../src/dropdown';

const Dropdown = new Component('Dropdown', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    wrapperClass: {
      type: String,
      defaultValue: 'dropdown',
      allowedValues: ['dropdown', 'group', 'prepend', 'append'],
    },
    direction: {
      type: String,
      optional: true,
      allowedValues: ['up', 'down', 'left', 'right'],
    },
    caret: {
      type: Boolean,
      defaultValue: true,
    },
    split: {
      type: Boolean,
      defaultValue: false,
    },
    right: {
      type: Boolean,
      defaultValue: false,
    },
    // данные для кнопки переключателя
    toggle: {
      type: Object,
      blackbox: true,
    },
    items: {
      type: Array,
      defaultValue: [],
    },
    'items.$': {
      type: DropdownItemSchema,
      optional: true,
    },
    // для работы dropdown как селект
    onSelect: {
      type: Function,
      optional: true,
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    wrapperClass() {
      const { wrapperClass, direction } = this.props;
      const wp = this.wrapper[wrapperClass];
      return {
        [wp]: true,
        [`drop${direction}`]: direction,
      };
    },
    toggle() {
      const { caret, toggle, split } = this.props;
      const { className, attributes } = toggle;
      return {
        ...toggle,
        className: c({ [`${this.cp}-toggle`]: caret && !split, className }),
        attributes: {
          ...attributes,
          ...((split && {}) || this.dataDropdown),
        },
      };
    },
    toggleSplit() {
      // цвет и размер кнопки
      const { color, size } = this.props.toggle;
      return {
        color,
        size,
        className: c({
          [`${this.cp}-toggle`]: true,
          [`${this.cp}-toggle-split`]: true,
        }),
        attributes: this.dataDropdown,
      };
    },
    menu() {
      return c({
        [`${this.cp}-menu`]: true,
        [`${this.cp}-menu-right`]: this.props.right,
      });
    },
    items() {
      const { items, onSelect } = this.props;
      if (!onSelect) {
        return items;
      }

      return items.map((item) => {
        const { onClick } = item;
        // обработка нажатия как у селекта
        item.onClick = (value, content) => {
          onSelect(value, content);
          // если нужна дополнительная обработка клика меню
          if (onClick) {
            onClick(value, content);
          }
        };
        return item;
      });
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'dropdown',
    dataDropdown: {
      'data-toggle': 'dropdown',
      'aria-haspopup': 'true',
      'aria-expanded': 'false',
    },
    wrapper: {
      dropdown: 'dropdown',
      group: 'btn-group',
      prepend: 'input-group-prepend',
      append: 'input-group-append',
    },
  },
});

export { Dropdown as default, DropdownItemSchema };
