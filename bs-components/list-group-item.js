import c from 'classnames';
import SimpleSchema from 'simpl-schema';
import Component from '../component';

// import template
import './list-group-item.html';

const NavLink = new Component('ListGroupItem', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    active: { type: Boolean, defaultValue: false },
    disabled: { type: Boolean, defaultValue: false },
    tag: { type: String, defaultValue: 'li', allowedValues: ['li', 'a', 'button'] },
    color: {
      type: String,
      optional: true,
      allowedValues: Component.colors,
    },
    href: {
      type: String,
      optional: true,
      autoValue() {
        if (this.field('tag').value === 'a' && !this.isSet) {
          return '#';
        }
      },
    },
    onClick: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    isButton() {
      return this.props.tag === 'button' && !this.props.href;
    },
    isA() {
      return this.props.tag === 'a' || this.props.href;
    },
    isLi() {
      return this.props.tag === 'li' && !this.props.href;
    },
    attributes() {
      const {
        active, disabled, tag, color, href, className, attributes,
      } = this.props;

      const classes = c({
        [this.cp]: true,
        [`${this.cp}-action`]: tag !== 'li',
        [`${this.cp}-${color}`]: color,
        disabled,
        active,
        [`${className}`]: className,
      });
      return {
        href,
        class: classes,
        disabled: tag === 'button' && disabled,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'click .list-group-item'(e) {
      const { disabled, href, onClick } = this.props;
      if (disabled) {
        e.preventDefault();
        return;
      }
      if (href === '#') {
        e.preventDefault();
      }
      if (onClick) {
        onClick();
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'list-group-item',
  },
});

export default NavLink;
