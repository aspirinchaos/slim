import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../component';
// import template
import './list-group.html';

const Nav = new Component('ListGroup', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    tag: { type: String, defaultValue: 'li', allowedValues: ['li', 'a', 'button'] },
    flush: { type: Boolean, defaultValue: false },
    items: { type: Array, defaultValue: [] },
    'items.$': { type: Object, blackbox: true },
    onClick: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    list() {
      return this.props.tag === 'li';
    },
    attributes() {
      const {
        flush, className, attributes,
      } = this.props;
      const classes = c({
        [this.cp]: true,
        [`${this.cp}-flush`]: flush,
        [`${className}`]: className,
      });
      return {
        class: classes,
        ...attributes,
      };
    },
    items() {
      const { items, onClick, tag } = this.props;
      return items.map((item) => {
        if (onClick) {
          item.onClick = () => onClick(item);
        }
        item.tag = tag;
        return item;
      });
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'list-group',
  },
});

export default Nav;
