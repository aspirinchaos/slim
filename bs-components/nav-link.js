import c from 'classnames';
import SimpleSchema from 'simpl-schema';
import Component from '../component';

// import template
import './nav-link.html';

const NavLink = new Component('NavLink', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    active: { type: Boolean, defaultValue: false },
    disabled: { type: Boolean, defaultValue: false },
    onClick: { type: Function, optional: true },
    href: {
      type: String,
      optional: true,
      autoValue() {
        if (!this.isSet) {
          return '#';
        }
      },
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        active, disabled, href, className, attributes,
      } = this.props;

      const classes = c({
        [this.cp]: true,
        disabled,
        active,
        [`${className}`]: className,
      });
      return {
        href,
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'click a'(e) {
      const { disabled, href, onClick } = this.props;
      if (disabled) {
        e.preventDefault();
        return;
      }
      if (href === '#') {
        e.preventDefault();
      }
      if (onClick) {
        onClick();
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'nav-link',
  },
});

export default NavLink;
