import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../component';
// import template
import './nav.html';

const sizeClasses = ['xs', 'sm', 'md', 'lg', 'xl'];

const Nav = new Component('Nav', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    vertical: {
      type: SimpleSchema.oneOf(Boolean, String),
      allowedValues: sizeClasses,
      defaultValue: false,
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        vertical, className, attributes,
      } = this.props;
      const classes = c({
        [this.cp]: true,
        [this.getVerticalClass()]: vertical,
        [`${className}`]: className,
      });
      return {
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'nav',
    getVerticalClass() {
      const { vertical } = this.props;
      if (vertical === true || vertical === 'xs') {
        return 'flex-column';
      }

      return `flex-${vertical}-column`;
    },
  },
});

export default Nav;
