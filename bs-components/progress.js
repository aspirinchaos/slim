import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../component';
// import template
import './progress.html';

const Progress = new Component('Progress', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    height: { type: SimpleSchema.Integer, optional: true },
    width: {
      type: SimpleSchema.Integer, defaultValue: 0, min: 0, max: 100,
    },
    color: { type: String, optional: true, allowedValues: Component.colors },
    striped: { type: Boolean, defaultValue: false },
    animated: { type: Boolean, defaultValue: false },
    label: { type: Boolean, defaultValue: false },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    wrapper() {
      const pr = {
        class: this.cp,
      };
      const { height } = this.props;
      if (height) {
        pr.style = `height: ${height}px`;
      }
      return pr;
    },
    progress() {
      const {
        width, color, striped, animated, className, attributes,
      } = this.props;
      const classes = c({
        [`${this.cp}-bar`]: true,
        [`bg-${color}`]: color,
        [`${this.cp}-striped`]: striped,
        [`${this.cp}-animated`]: animated,
        [`${className}`]: className,
      });
      return {
        style: `width: ${width}%`,
        class: classes,
        role: 'progressbar',
        'aria-valuenow': width,
        'aria-valuemin': 0,
        'aria-valuemax': 100,
        ...attributes,
      };
    },
    label() {
      const { width, label } = this.props;
      return (label && width && `${width}%`) || '';

    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'progress',
  },
});

export default Progress;
