import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../component';
import { vertical, prefixes } from './col';
// import template
import './row.html';

const horizontal = {
  type: String,
  optional: true,
  allowedValues: ['start', 'center', 'end', 'around', 'between'],
};

const SizeObject = new SimpleSchema({
  horizontal,
  vertical,
});

const Row = new Component('Row', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    noGutters: { type: Boolean, defaultValue: false },
    horizontal,
    vertical,
    // xs тут и не нужен, ведь классы для него будут равны
    // классам определяемым по параметрам выше
    // но для совместимости и красоты, пусть будет
    xs: { type: SizeObject, optional: true },
    sm: { type: SizeObject, optional: true },
    md: { type: SizeObject, optional: true },
    lg: { type: SizeObject, optional: true },
    xl: { type: SizeObject, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        noGutters, xs, sm, md, lg, xl, className, attributes,
      } = this.props;
      const classes = c(
        { [this.cp]: true, 'no-gutters': noGutters },
        this.getAlign(this.props, 0),
        [xs, sm, md, lg, xl].map(this.getAlign),
        className,
      );
      return {
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    // class parent
    cp: 'row',
    getAlign(size, index) {
      if (!size) {
        return '';
      }
      const { vertical: v, horizontal: h } = size;
      const prefix = prefixes[index];
      return {
        [`align-items${prefix}-${v}`]: v,
        [`justify-content${prefix}-${h}`]: h,
      };
    },
  },
});

export default Row;
