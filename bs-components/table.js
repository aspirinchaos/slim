import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../component';
// import template
import './table.html';

const Table = new Component('Table', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    dark: { type: Boolean, optional: true },
    striped: { type: Boolean, optional: true },
    bordered: { type: Boolean, optional: true },
    borderless: { type: Boolean, optional: true },
    hover: { type: Boolean, optional: true },
    small: { type: Boolean, optional: true },
    responsive: {
      type: SimpleSchema.oneOf(Boolean, String),
      optional: true,
      allowedValues: ['sm', 'md', 'lg', 'xl'],
    },
    caption: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        className, attributes, dark, striped, bordered, borderless,
        hover, small,
      } = this.props;
      const classes = c({
        [this.cp]: true,
        [`${this.cp}-dark`]: dark,
        [`${this.cp}-striped`]: striped,
        [`${this.cp}-bordered`]: bordered,
        [`${this.cp}-borderless`]: borderless,
        [`${this.cp}-hover`]: hover,
        [`${this.cp}-sm`]: small,
        [`${className}`]: className,
      });
      return {
        class: classes,
        ...attributes,
      };
    },
    responsive() {
      const { responsive: r } = this.props;
      return {
        class: `${this.cp}-responsive${r === true ? '' : `-${r}`}`,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'table',
  },
});

export default Table;
