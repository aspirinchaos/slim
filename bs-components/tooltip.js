import SimpleSchema from 'simpl-schema';
import Component from '../component';
// import template
import './tooltip.html';
// import bs js
import '../src/tooltip';

const Tooltip = new Component('Tooltip', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    placement: {
      type: String,
      allowedValues: ['auto', 'top', 'bottom', 'left', 'right'],
      defaultValue: 'top',
    },
    title: String,
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    // отобор по DOM элементам
    this.element = this.$(this.firstNode.nextElementSibling);
    this.autorun(() => {
      const { placement, title } = this.props;
      this.element.tooltip({
        placement, title,
      });
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

export default Tooltip;
