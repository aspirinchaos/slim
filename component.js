import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

/**
 * Общий класс для всех компонетов
 * todo@aspirin есть ли свособ определить contentBlock при рендере из js
 * todo@aspirin нужно ли будет сделать реактивный рендер
 * todo@aspirin правильный ли подход со схемой
 * @param name {string} имя тимплейта
 * @param settings {object} настройки для контроллера
 */
class Component {
  /**
   * Возвращает результат рендера blaze, т.е. чистый html
   * @type {function}
   * @param data
   * @returns {string}
   */
  render;

  /**
   * Схема компонента
   * @type {SimpleSchema}
   */
  schema;

  /**
   * Доступные цвета bootstrap
   * @type {string[]}
   */
  static colors = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'];

  constructor(name, settings) {
    // схема находится тут, потому extend мутабельный метод
    // а для каждого компонента нужна отдельная общая схема
    const ShareSchema = new SimpleSchema({
      // содержимое, используется вместо {{> Template.contentBlock }}
      // потому Template.contentBlock не задать через параметры
      content: { type: String, optional: true },
      // дополнительные классы для компонента
      className: { type: String, optional: true, defaultValue: '' },
      // дополнительные аттрибуты
      attributes: { type: Object, defaultValue: {}, blackbox: true },
    });
    if (settings.props) {
      ShareSchema.extend(settings.props);
    }
    settings.props = ShareSchema;
    this.schema = ShareSchema;
    this.render = TemplateController(name, settings);
  }
}

export default Component;
