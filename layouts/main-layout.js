import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './main-layout.html';

TemplateController('MainLayout', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    header: { type: Function, optional: true },
    headerLeft: { type: Function, optional: true },
    headerRight: { type: Function, optional: true },
    navbar: { type: Function, optional: true },
    fullWidth: { type: Function, optional: true },
    template: Function,
    footer: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    hasHeader() {
      return this.props.header || this.props.headerLeft || this.props.headerRight;
    },
    header() {
      return {
        template: this.props.header,
        data: {},
      };
    },
    headerLeft() {
      return {
        template: this.props.headerLeft,
        data: {},
      };
    },
    headerRight() {
      return {
        template: this.props.headerRight,
        data: {},
      };
    },
    navbar() {
      return {
        template: this.props.navbar,
        data: {},
      };
    },
    fullWidth() {
      return {
        template: this.props.fullWidth,
        data: {},
      };
    },
    template() {
      return {
        template: this.props.template,
        data: {},
      };
    },
    footer() {
      return {
        template: this.props.footer,
        data: {},
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
