import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './split-layout.html';

TemplateController('SplitLayout', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    right: Function,
    left: Function,
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    right() {
      return {
        template: this.props.right,
        data: {},
      };
    },
    left() {
      return {
        template: this.props.left,
        data: {},
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
