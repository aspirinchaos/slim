Package.describe({
  name: 'slim',
  version: '0.1.7',
  summary: 'Responsive Bootstrap 4 Admin Template',
  git: 'https://bitbucket.org/aspirinchaos/slim.git',
  documentation: 'README.md',
});

Npm.depends({
  bootstrap: '4.1.2',
});

Package.onUse((api) => {
  api.versionsFrom('1.7');
  api.use([
    'ecmascript',
    'fourseven:scss',
    'templating',
    'jquery',
    'template-controller',
    'tomi:upload-jquery@2.4.0',
    'tmeasday:check-npm-versions',
  ]);
  api.mainModule('slim.js', 'client');
  api.addAssets([
    'fonts/roboto-light.ttf',
    'fonts/roboto-regular.ttf',
    'fonts/roboto-medium.ttf',
    'fonts/roboto-bold.ttf',
    'fonts/open-sans-light.ttf',
    'fonts/open-sans-regular.ttf',
    'fonts/open-sans-semi-bold.ttf',
    'fonts/open-sans-bold.ttf',
    'fonts/montserrat-light.ttf',
    'fonts/montserrat-regular.ttf',
    'fonts/montserrat-medium.ttf',
    'fonts/montserrat-semi-bold.ttf',
    'fonts/montserrat-bold.ttf',
    'fonts/lato-light.ttf',
    'fonts/lato-regular.ttf',
    'fonts/lato-bold.ttf',
  ], 'client');
  api.addFiles('styles/slim.scss', 'client');
  api.addFiles('styles/bs/_functions.scss', 'client');
  api.addFiles('styles/bs/_variables.scss', 'client');
  api.addFiles('styles/bs/_mixins.scss', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('slim');
  api.mainModule('slim-tests.js');
});
