import Component from './component';

class SlimComponent extends Component {
  /**
   * Доступные цвета slim
   * @type {string[]}
   */
  static colors = [...Component.colors, 'teal', 'indigo', 'purple', 'orange', 'pink'];
}

export default SlimComponent;
