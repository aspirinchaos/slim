import SimpleSchema from 'simpl-schema';
import SlimComponent from '../slim-component';
// import template
import './slim-card-header.html';

const SlimCardHeader = new SlimComponent('SlimCardHeader', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    header: { type: Boolean, defaultValue: false },
    tag: { type: String, defaultValue: 'h6', allowedValues: ['h6', 'label', 'div'] },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    h6() {
      return this.props.tag === 'h6';
    },
    label() {
      return this.props.tag === 'label';
    },
    div() {
      return this.props.tag === 'div';
    },
    attributes() {
      return {
        class: `slim-card-title ${this.props.className}`,
        ...this.props.attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

export default SlimCardHeader;
