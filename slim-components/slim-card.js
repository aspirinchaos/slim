import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import SlimComponent from '../slim-component';
import Card from '../bs-components/card/card';
// import template
import './slim-card.html';

const SlimCard = new SlimComponent('SlimCard', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    title: { type: String, defaultValue: '' },
    dashOne: { type: Boolean, defaultValue: false },
    table: { type: Boolean, defaultValue: false },
    info: { type: Boolean, defaultValue: false },
    sales: { type: Boolean, defaultValue: false },
    impression: { type: Boolean, defaultValue: false },
    invoice: { type: Boolean, defaultValue: false },
  }).extend(Card.schema.pick(['color', 'outline', 'inverse'])),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    slimAdditional() {
      const {
        dashOne, table, info, sales, impression, invoice, className,
      } = this.props;
      return {
        ...this.props,
        className: c({
          [`${this.cp}-dash-one`]: dashOne,
          [`${this.cp}-table`]: table,
          [`${this.cp}-info`]: info,
          [`${this.cp}-sales`]: sales,
          [`${this.cp}-impression`]: impression,
          [`${this.cp}-invoice`]: invoice,
          [`${className}`]: className,
        }),

      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'card',
  },
});

export default SlimCard;
