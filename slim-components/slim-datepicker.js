import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import 'jquery-ui/ui/widgets/datepicker';
import 'jquery-ui/ui/i18n/datepicker-ru';
import SlimComponent from '../slim-component';

// import template
import './slim-datepicker.html';

const SlimDatepicker = new SlimComponent('SlimDatepicker', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    color: {
      type: String,
      allowedValues: SlimComponent.colors,
      optional: true,
    },
    value: {
      type: String,
      optional: true,
    },
    id: {
      type: String,
      optional: true,
    },
    placeholder: {
      type: String,
      optional: true,
    },
    name: String,
    groupClassName: {
      type: String,
      defaultValue: '',
    },
    onSelect: {
      type: Function,
      defaultValue: (text, inst) => {
      },
    },
    handlePicker: {
      type: Function,
      optional: true,
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    // все параметры http://api.jqueryui.com/datepicker/
    const { color, handlePicker, onSelect } = this.props;
    const classes = c({
      [this.cp]: true,
      [`${this.cp}-color`]: !!color,
      [`${this.cp}-${color}`]: !!color,
    });
    this.picker = this.$('input').datepicker({
      dateFormat: 'dd.mm.yy',
      beforeShow(input, inst) {
        inst.dpDiv.addClass(classes);
      },
      onSelect,
    });
    if (handlePicker) {
      handlePicker(this.picker);
    }
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    groupClass() {
      return c('input-group', this.props.groupClassName);
    },
    attributes() {
      const {
        value, name, id, placeholder,
      } = this.props;
      return {
        id,
        name,
        placeholder,
        type: 'text',
        class: 'form-control',
        value,
        autocomplete: 'off',
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'fc-datepicker',
    picker: null,
  },
});

export default SlimDatepicker;
