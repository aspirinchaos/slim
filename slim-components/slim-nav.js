import c from 'classnames';
import SlimComponent from '../slim-component';
import Nav from '../bs-components/nav';

// import template
import './slim-nav.html';

const SlimNav = new SlimComponent('SlimNav', {
  // Validate the properties passed to the template from parents
  props: Nav.schema,

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    slimAdditional() {
      const {
        // не используем дефолт параметр, так как он ставится только при undefined
        vertical, className,
      } = this.props;
      return {
        ...this.props,
        // обязательный параметр, если передан используем переданный
        vertical: vertical || true,
        className: c({
          [this.cp]: true,
          [`${className}`]: className,
        }),

      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'slim-nav',
  },
});

export default SlimNav;
