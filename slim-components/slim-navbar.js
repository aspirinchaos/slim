import { TemplateController } from 'meteor/template-controller';
import { FlowRouter } from 'meteor/kadira:flow-router';
import SimpleSchema from 'simpl-schema';
import c from 'classnames';
// import template
import './slim-navbar.html';

const SubItem = new SimpleSchema({
  title: String,
  // имя роута
  route: { type: String, optional: true },
  params: { type: Object, defaultValue: {} },
});

const MenuItem = new SimpleSchema({
  icon: { type: String, optional: true },
  title: String,
  after: { type: String, optional: true },
  // имя роута
  route: { type: SimpleSchema.oneOf(String, Array), optional: true },
  // для формирования ссылки используется 1 роут, остальные только
  // для подсвечивания меню!
  'route.$': { type: String, optional: true },
  params: { type: Object, defaultValue: {} },
  // Контент для мега меню
  mega: { type: String, optional: true },
  subitems: { type: Array, optional: true },
  'subitems.$': SubItem,
  'subitems.$.subitems': { type: Array, optional: true },
  'subitems.$.subitems.$': SubItem,
});

const isActive = (item) => {
  const routes = [];
  if (Array.isArray(item.route)) {
    routes.push(...item.route);
  } else if (item.route) {
    routes.push(item.route);
  }
  if (item.subitems) {
    item.subitems.forEach((subitem) => {
      if (subitem.route) {
        routes.push(subitem.route);
      }
    });
  }
  return routes.some(name => name === FlowRouter.getRouteName());
};

TemplateController('SlimNavbar', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    items: Array,
    'items.$': MenuItem,
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    itemClass(item) {
      return c({
        'nav-item': true,
        'with-sub': item.subitems || item.mega,
        'mega-dropdown': item.mega,
        active: isActive(item),
      });
    },
    path(item) {
      let { route } = item;
      if (Array.isArray(route)) {
        [route] = route;
      }
      if (!route) {
        return '#';
      }
      // Уже сформированный путь
      if (route.includes('/')) {
        return route;
      }
      return FlowRouter.path(route, item.params);
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
