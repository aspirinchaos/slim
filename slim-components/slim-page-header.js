import { FlowRouter } from 'meteor/kadira:flow-router';
import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import SlimComponent from '../slim-component';
// import template
import './slim-page-header.html';

const SlimPageHeader = new SlimComponent('SlimPageHeader', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    title: { type: String, optional: true },
    links: { type: Array, defaultValue: [] },
    'links.$': SimpleSchema.oneOf(Object, String),
    'links.$.route': { type: String, optional: true },
    'links.$.params': { type: Object, defaultValue: {} },
    'links.$.title': String,
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        className, attributes,
      } = this.props;
      const classes = c({
        [this.cp]: true,
        [`${className}`]: className,
      });
      return {
        class: classes,
        ...attributes,
      };
    },
    links() {
      const length = this.props.links.length - 1;
      return this.props.links.map((link, index) => {
        const resultLink = {
          attributes: {
            class: `breadcrumb-item ${(length === index && 'active') || ''}`,
            // только для последнего элемента
            'aria-current': (length === index && 'page') || '',
          },
          path: (length !== index && '#') || '',
        };
        if (typeof link === 'string') {
          resultLink.title = link;
          return resultLink;
        }
        const { route, params, title } = link;
        resultLink.title = title;
        if (route && length !== index) {
          resultLink.path = FlowRouter.path(route, params);
        }
        return resultLink;
      });
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'slim-pageheader',
  },
});

export default SlimPageHeader;
