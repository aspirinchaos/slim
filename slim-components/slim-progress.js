import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Progress from '../bs-components/progress';
import SlimComponent from '../slim-component';
// import template
import './slim-progress.html';

const SlimProgress = new SlimComponent('SlimProgress', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    color: {
      type: String,
      allowedValues: SlimComponent.colors,
      optional: true,
    },
    size: {
      type: String,
      allowedValues: ['xs', 'sm', 'lg'],
      optional: true,
    },
  }).extend(Progress.schema.pick('height', 'width', 'striped', 'animated')),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const { color, size, className } = this.props;
      const attributes = { ...this.props };
      delete attributes.color;
      attributes.className = c({
        [`bg-${color}`]: color,
        [`${Progress.cp}-${size}`]: size,
        [`${className}`]: className,
      });
      return attributes;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

export default SlimProgress;
