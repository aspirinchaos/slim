import { TemplateController } from 'meteor/template-controller';
import { FA } from 'meteor/font-awesome';
import { _t } from 'meteor/i18n';
import SimpleSchema from 'simpl-schema';
// import template
import './slim-search.html';

TemplateController('SlimSearch', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    value: { type: String, defaultValue: '' },
    handleOEM: Function,
    handleName: Function,
  }),

  // Setup private reactive template state
  state: {
    text: '',
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    input() {
      return {
        type: 'text',
        class: 'form-control',
        placeholder: _t('Поиск'),
        value: this.props.value,
      };
    },
    search() {
      return {
        color: 'primary',
        content: FA({ name: 'search' }),
        onClick: () => this.handleSearch(),
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'input input'(e) {
      this.state.text = e.currentTarget.value;
    },
    'keyup input'(e) {
      const keyCode = e.keyCode || e.which;
      if (keyCode === 13) {
        this.handleSearch();
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    /**
     * Частотность чисел в строке поиска для определения OEM
     */
    frequency: 0.2,
    /**
     * Проверка строки OEM или название
     * true - если это OEM, false - если наименование
     * @return {boolean}
     */
    analyze(text) {
      const numbers = text.replace(/[^0-9]/g, '').length;
      return (numbers / text.length) > this.frequency;
    },
    /**
     * Обработка поиска введенного значения
     */
    handleSearch() {
      const { text } = this.state;
      if (!text) {
        return;
      }
      if (this.analyze(text)) {
        this.props.handleOEM(text);
      } else {
        this.props.handleName(text);
      }
    },
  },
});
