import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import SlimComponent from '../slim-component';
// import template
import './slim-section-wrapper.html';

const SlimSectionWrapper = new SlimComponent('SlimSectionWrapper', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    title: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        className, attributes,
      } = this.props;
      const classes = c({
        [this.cp]: true,
        [`${className}`]: className,
      });
      return {
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    // class parent
    cp: 'section-wrapper',
  },
});

export default SlimSectionWrapper;
