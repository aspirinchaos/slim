import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import SlimComponent from '../slim-component';
// import template
import './slim-square.html';

const SlimSquare = new SlimComponent('SlimSquare', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    size: { type: Number, defaultValue: 8, allowedValues: [8, 10] },
    rounded: { type: Boolean, defaultValue: true },
    color: {
      type: String,
      defaultValue: 'primary',
      allowedValues: SlimComponent.colors,
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        size, color, rounded, className, attributes,
      } = this.props;
      return {
        class: c({
          [`square-${size}`]: true,
          [`bg-${color}`]: true,
          'rounded-circle': rounded,
          [`${className}`]: className,
        }),
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

export default SlimSquare;
