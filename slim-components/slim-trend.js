import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import SlimComponent from '../slim-component';
// import template
import './slim-trend.html';

const SlimTrend = new SlimComponent('SlimTrend', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    type: { type: String, allowedValues: ['up', 'down'] },
    reverse: { type: Boolean, defaultValue: false },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        type, reverse, attributes, className,
      } = this.props;
      const color = (((type === 'up' && !reverse) || (type === 'down' && reverse)) && 'success') || 'danger';
      return {
        class: c({
          [`text-${color}`]: true,
          [`${className}`]: className,
        }),
        ...attributes,
      };
    },
    isUp() {
      return this.props.type === 'up';
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

export default SlimTrend;
