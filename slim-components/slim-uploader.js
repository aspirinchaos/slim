import SimpleSchema from 'simpl-schema';
import { _t } from 'meteor/i18n';
import c from 'classnames';
import { Uploader } from 'meteor/tomi:upload-jquery';
import SlimComponent from '../slim-component';
// import template
import './slim-uploader.html';

const SlimUploader = new SlimComponent('SlimUploader', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    color: { type: String, optional: true, allowedValues: ['primary', 'inverse'] },
    accept: { type: String, defaultValue: 'image/*' },
    afterUpload: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    Uploader.init(this);
    if (this.props.afterUpload) {
      Uploader.finished = (index, file) => {
        this.info.set('');
        this.props.afterUpload(file);
      };
    }
  },
  onRendered() {
    Uploader.render.call(this);
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    labelClass() {
      const { color } = this.props;
      return c({
        [this.cp]: true,
        [`${this.cp}-${color}`]: color,
        start: true,
      });
    },
    infoLabel() {
      // we may have not yet selected a file
      const info = this.info.get();
      if (!info) {
        return _t('Выберите файл');
      }

      const progress = this.globalInfo.get();

      // we display different result when running or not
      return progress.running ?
        `${info.name} - ${progress.progress}% - [${progress.bitrate}]` :
        info.name;
    },
    upload() {
      return this.info.get();
    },
    startUpload() {
      return {
        type: 'submit',
        content: _t('Загрузить'),
        color: 'success',
        onClick: () => {
          Uploader.startUpload.call(this);
        },
      };
    },
    running() {
      return this.globalInfo.get().running;
    },
    progress() {
      return this.globalInfo.get().progress;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'custom-file-label',
  },
});

export default SlimUploader;
