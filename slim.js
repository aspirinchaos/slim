import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
// utils
import Component from './component';
import SlimComponent from './slim-component';
// slim layouts
import './layouts/blank-layout';
import './layouts/error-layout';
import './layouts/main-layout';
import './layouts/simple-layout';
import './layouts/split-layout';
// bs components
import Card from './bs-components/card/card';
import CardContent from './bs-components/card/card-content';
import CardLayout from './bs-components/card/card-layout';
import Dropdown from './bs-components/dropdown/dropdown';
import Modal from './bs-components/modal/modal';
import ModalBody from './bs-components/modal/modal-body';
import ModalFooter from './bs-components/modal/modal-footer';
import ModalHeader from './bs-components/modal/modal-header';
import Alert from './bs-components/alert';
import Button from './bs-components/button';
import ButtonGroup from './bs-components/button-group';
import Col from './bs-components/col';
import Container from './bs-components/container';
import ListGroup from './bs-components/list-group';
import ListGroupItem from './bs-components/list-group-item';
import Nav from './bs-components/nav';
import NavLink from './bs-components/nav-link';
import Progress from './bs-components/progress';
import Row from './bs-components/row';
import Table from './bs-components/table';
import Tooltip from './bs-components/tooltip';
// slim components
import SlimCard from './slim-components/slim-card';
import SlimCardHeader from './slim-components/slim-card-header';
import SlimDatepicker from './slim-components/slim-datepicker';
import SlimNav from './slim-components/slim-nav';
import SlimPageHeader from './slim-components/slim-page-header';
import SlimProgress from './slim-components/slim-progress';
import SlimSectionWrapper from './slim-components/slim-section-wrapper';
import SlimSquare from './slim-components/slim-square';
import SlimTrend from './slim-components/slim-trend';
import SlimUploader from './slim-components/slim-uploader';
// slim template only
import './slim-components/slim-navbar';
import './slim-components/slim-search';

checkNpmVersions({
  'simpl-schema': '1.5.3',
  classnames: '2.2.6',
  jquery: '3.3.1',
  'popper.js': '1.14.3',
  'jquery-ui': '1.12.1',
}, 'slim');

export {
  // utils
  Component,
  SlimComponent,
  // bs
  Card,
  CardContent,
  CardLayout,
  Dropdown,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Alert,
  Button,
  ButtonGroup,
  Col,
  Container,
  ListGroup,
  ListGroupItem,
  Nav,
  NavLink,
  Progress,
  Row,
  Table,
  Tooltip,
  // slim
  SlimCard,
  SlimCardHeader,
  SlimDatepicker,
  SlimNav,
  SlimPageHeader,
  SlimProgress,
  SlimSectionWrapper,
  SlimSquare,
  SlimTrend,
  SlimUploader,
};
